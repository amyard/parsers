---

## Memorial
### Python3.6, Scrapy, Selenium



1. **odb_memorial_spider.py** - crawling with scrapy https://obd-memorial.ru  .
2. **pamjatj_selenium_full** - using selenium webdriver for parsing https://pamyat-naroda.ru . using additional js script for download images from cdn.

---

## Whoro
### Python2.7, Scrapy

There are 13 spiders for collecting data.
Simplest django templates for review new persons.
---
