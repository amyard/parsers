# -*- coding: utf-8 -*-

from sqlalchemy.orm import sessionmaker
from .models import db_connect, create_table, Information
import psycopg2


class MemorialPipeline(object):
    def __init__(self):
        """
        Initializes database connection and sessionmaker
        Creates tables
        """
        engine = db_connect()
        create_table(engine)
        self.Session = sessionmaker(bind=engine)


    def process_item(self, item, spider):
        """Save info in the database
        This method is called for every item pipeline component
        """
        session = self.Session()
        info = Information()

        # user info
        info.ids = item["ids"]
        info.name = item["name"]
        info.surname = item["surname"]
        info.last_name = item["last_name"]
        info.birthday = item["birthday"]
        info.birthday_oblastj = item["birthday_oblastj"]
        info.birthday_rajon = item["birthday_rajon"]
        info.birthday_city = item["birthday_city"]
        info.person_alive = item["person_alive"]
        info.photo = item["photo"]

        # reward info
        info.name_of_reward = item['name_of_reward']
        info.reward_alive = item["reward_alive"]
        info.timestamp_get_reward = item["timestamp_get_reward"]
        info.reasons_for_disposal = item['reasons_for_disposal']
        info.date_for_disposal = item['date_for_disposal']
        info.dead_oblastj = item["dead_oblastj"]
        info.dead_rajon = item["dead_rajon"]
        info.dead_city = item["dead_city"]
        info.dead_additional_info = item["dead_additional_info"]
        info.zvanie = item["zvanie"]
        info.military_station = item["military_station"]
        info.name_of_reporting_source = item["name_of_reporting_source"]
        info.nmb_of_reporting_source = item["nmb_of_reporting_source"]
        info.nmb_description_of_reporting_source = item["nmb_description_of_reporting_source"]
        info.nmb_case_of_reporting_source = item["nmb_case_of_reporting_source"]
        info.url_of_source = item["url_of_source"]

        # common
        info.url_of_parsed_page = item['url_of_parsed_page']
        info.additional_info = item['additional_info']

        try:
            session.add(info)
            session.commit()

        except:
            session.rollback()
            raise

        finally:
            session.close()

        return item
