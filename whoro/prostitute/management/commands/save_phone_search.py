from django.core.management.base import BaseCommand, CommandError
from prostitute.models import ProstituteModel


class Command(BaseCommand):

    def handle(self, *args, **kwargs):
        for instance in ProstituteModel._default_manager.all():
            instance.save()
            print(instance.phone_search, instance.phone_new)


