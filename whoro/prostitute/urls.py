from django.conf.urls import url, include
from . import views


urlpatterns = [
    url(r'^detail/(?P<pk>\d+)$', views.GirlsDetailView.as_view(), name='detail'),
    url(r'^$', views.HomeRedirectView.as_view(), name='home'),
    url(r'^filter/(?P<filter>\w+|\d)$', views.GirlsListView.as_view(), name='filter'),
    url(r'^xlsx/(?P<filter>\w+|\d)$', views.xlsx, name='xlsx'),
]
