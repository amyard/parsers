# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
from django.db import models
# from django.db.models.signals import pre_save
# from django.dispatch import receiver
import re

SITE_NAME = (
    (1, u'top_modals'),
    (2, u'relaxportal'),
    (3, u'ukrgo'),
    (4, u'minuet'),
    (5, u'sexzakaz'),
    
    (6, u'kievsex'),
    (7, u'sexkiev'),
    (8, u'sexua'),
    (9, u'escortkiev'),
    (10, u'intimgirls'),
    (11, u'kievlady'),
    (12, u'hot_love'),
    (13, u'kiev_girls'),
)


class ProstituteModel(models.Model):
    GENDER = (
        (1, u'female'),
        (2, u'male'),
    )
    name = models.CharField('name', max_length=128, blank=True)
    phone = models.CharField('phone', max_length=128, blank=True)
    phone_search = models.PositiveIntegerField('phone number for search', blank=True, null=True)
    phone_new = models.BooleanField(default=False)
    price_1 = models.PositiveIntegerField('price one hour', blank=True, null=True)
    price_2 = models.PositiveIntegerField('price two hours', blank=True, null=True)
    price_night = models.PositiveIntegerField('price all night', blank=True, null=True)
    currency = models.CharField('currency', max_length=8, blank=True)
    city = models.CharField('city', max_length=128, blank=True)
    district = models.CharField('district', max_length=1024, blank=True)
    underground = models.CharField('closest underground', max_length=1024, blank=True)
    proposes = models.TextField('proposes', blank=True)
    different = models.TextField('different', blank=True)
    description = models.TextField('description', blank=True)
    weight = models.PositiveIntegerField('price', blank=True, null=True)
    height = models.PositiveIntegerField('price', blank=True, null=True)
    bust = models.PositiveIntegerField('price', blank=True, null=True)
    age = models.PositiveIntegerField('price', blank=True, null=True)
    url = models.CharField('url', max_length=256, blank=True)
    image_urls = models.TextField('image_urls', blank=True)
    date = models.DateTimeField(auto_now_add=True)

    site_number = models.PositiveSmallIntegerField(choices=SITE_NAME, blank=True, null=True)
    gender = models.PositiveSmallIntegerField(choices=GENDER, blank=True, default=1)

    def save(self):
        if self.phone:
            numbers = ''.join(re.findall(r'\d+', self.phone))[-9:]
            self.phone_search = int(numbers) if numbers else 0
            if self.phone_search:
                self.phone_new = not ProstituteModel.objects.filter(phone_search=self.phone_search).exists()
        super(ProstituteModel, self).save()

    @property
    def photo(self):
        return json.loads(self.image_urls)[0]

    @property
    def photos(self):
        return json.loads(self.image_urls) if self.image_urls else ''

    @property
    def different_load(self):
        return json.loads(self.different) if self.different else ''

    @property
    def proposes_load(self):
        return json.loads(self.proposes) if self.proposes else ''

    @property
    def date_str(self):
        return self.date.strftime('%Y-%m-%d %H-%M')

    def __unicode__(self):
        return u'{}{}'.format(self.name, str(self.price_1))


CITIES = {
    u'vinnits': u'vinnytsia',
    u'dnepropetrovsk': u'dnipropetrovsk',
    u'donets': u'donetsk',
    u'zhitomir': u'zhytomyr',
    u'zapor': u'zaporizhzhia',
    u'ivano': u'ivano-frankivsk',
    u'kiev': u'kyiv',
    u'kirovograd': u'kirovohrad',
    u'krivoy': u'krivoy-rog',
    u'lugansk': u'luhansk',
    u'luts': u'lutsk',
    u'lviv': u'lviv',
    u'lvov': u'lviv',
    u'nikolaev': u'mykolaiv',
    u'odes': u'odesa',
    u'poltav': u'poltava',
    u'rovno': u'rivne',
    u'sevastopo': u'sevastopol',
    u'simferopo': u'simferopol',
    u'sum': u'sumy',
    u'ternop': u'ternopil',
    u'uzhgoro': u'uzhhorod',
    u'khark': u'kharkiv',
    u'harkov': u'kharkiv',
    u'herson': u'kherson',
    u'hmelnitsk': u'khmelnytskyi',
    u'cherkas': u'cherkasy',
    u'chernovtsy': u'chernivtsi',
    u'chernigov': u'chernihiv',
}

Cityes_hot_love = {
    u'Киев': u'kyiv',
    u'Днепропетровск': u'dnipropetrovsk',
    u'Донецк': u'donetsk',
    u'Харьков': u'kharkiv',
    u'Одесса': u'odesa',
    u'Запорожье': u'zaporizhzhia',
    u'Николаев': u'mykolaiv',
    u'Симферополь': u'simferopol',
    u'Ивано-Франковск': u'ivano-frankivsk',
    u'Кривой Рог': u'krivoy-rog',
    u'Львов': u'lviv',
    u'Полтава': u'poltava',
    u'Сумы': u'sumy',
    u'Тернополь': u'ternopil',
    u'Херсон': u'kherson',

    u'Винница': u'vinnytsia',
    u'Житомир': u'zhytomyr',
    u'Кировоград': u'kirovohrad',
    u'Луганск': u'luhansk',
    u'Луцк': u'lutsk',
    u'Ровно': u'rivne',
    u'Севастополь': u'sevastopol',
    u'Ужгород': u'uzhhorod',
    u'Хмельницкий': u'khmelnytskyi',
    u'Черкассы': u'cherkasy',
    u'Черновцы': u'chernivtsi',
    u'Чернигов': u'chernihiv',
}