# -*- coding: utf-8 -*-
from .models import ProstituteModel
from pornoSites import settings
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.base import RedirectView
from datetime import datetime, date, timedelta
from prostitute.models import SITE_NAME
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseNotFound
from datetime import date

# import xlwt
import base64
import urlparse
import io
from xlsxwriter.workbook import Workbook
import urllib2
# from excel_response import ExcelResponse
from django.utils.html import strip_tags

from django.db.models import Count



class HomeRedirectView(RedirectView):
    def get_redirect_url(self):
        return reverse('filter', args=['new_numbers'])


def get_prostitute_queryset(selection):
        queryset = ProstituteModel._default_manager.none()
        if selection == 'all':
            queryset = ProstituteModel._default_manager.filter(city='kyiv')
        elif selection == 'fresh':
            queryset = ProstituteModel._default_manager.filter(
                        date__startswith=date.today()
                        ).filter(city='kyiv')
        elif selection == 'new_three_days':
            startdate = datetime.now() - timedelta(days=3)
            queryset = ProstituteModel._default_manager.filter(
                        date__range=[startdate, date.today()]
                        ).filter(city='kyiv')
        elif selection == 'new_numbers':
            queryset = ProstituteModel._default_manager.filter(
                        date__startswith=date.today()
                        ).filter(city='kyiv').filter(phone_new=True)
        else:
            for key, value in SITE_NAME:
                if selection == value:
                    queryset = ProstituteModel._default_manager.filter(city='kyiv')\
                                              .filter(site_number=int(key))
                    break
                if selection == value+'unique':
                    all = ProstituteModel.objects.values('phone_search').annotate(dcount=Count('phone_search'))
                    unique_phones = []
                    for i in all:
                        if i['dcount']==1:
                            unique_phones.append(i['phone_search'])
                    queryset = ProstituteModel.objects.filter(site_number=int(key)).filter(phone_search__in=unique_phones)
                    break
        return queryset.order_by('-date')


class GirlsListView(ListView):
    template_name = 'list.html'
    model = ProstituteModel
    paginate_by = 40

    def get_queryset(self):
        if self.kwargs['filter']:
            queryset = get_prostitute_queryset(self.kwargs['filter'])
        else:
            queryset = ProstituteModel._default_manager.none()
        print(len(queryset))
        return queryset

    def get_context_data(self, **kwargs):
        context = super(GirlsListView, self).get_context_data(**kwargs)
        context['SITE_NAME'] = SITE_NAME
        context['filter'] = self.kwargs['filter'] if 'filter' in self.kwargs else 'new_numbers'
        return context


class GirlsDetailView(DetailView):
    template_name = 'detail.html'
    model = ProstituteModel

    # def get_context_data(self, **kwargs):
    #     pass


def xlsx(request, filter):
    output = io.BytesIO()
    workbook = Workbook(output, {'in_memory': True})
    bold = workbook.add_format({'bold': True})
    worksheet = workbook.add_worksheet()
    worksheet.set_column('A:E', 20)
    merge_format = workbook.add_format({
                                        'bold': 1,
                                        'border': 1,
                                        'align': 'center',
                                        'valign': 'vcenter',
                                        'fg_color': 'yellow'})
    queryset = get_prostitute_queryset(filter)
    # worksheet.merge_range('A2:M2',"Product Name :"+product.name, merge_format)
    heading = [u"Имя", u"Номер телефона", u"Город", u"Цена час"]
    for counter, head in enumerate(heading):
        worksheet.write(0, counter, head, merge_format)
    for row, query in enumerate(queryset):
        # information = [str(query.name).decode('utf-8'),
        #                str(query.phone).decode('utf-8'),
        #                str(query.city).decode('utf-8')]
        information = [query.name, query.phone, query.city, query.price_1]
        for counter, info in enumerate(information):
            worksheet.write(row+1, counter, info)
    workbook.close()
    output.seek(0)
    response = HttpResponse(output.read(), content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    response['Content-Disposition'] = "attachment; filename=%s_%s.xlsx" % (date.today().strftime('%Y-%m-%d'), filter)
    return response

