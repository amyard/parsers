# -*- coding: utf-8 -*-
from ..items import LadieItem
import datetime, json, re, scrapy
from scrapy.spiders import CrawlSpider, Rule, Spider, SitemapSpider
from scrapy.linkextractors import LinkExtractor
import urlparse
from scrapy import Selector
from prostitute.models import ProstituteModel


class KievLadySpider(Spider):
    name = 'kievlady'
    allowed_domains = ['kievlady.com']
    site_number = 11
    start_urls = ['http://kievlady.com/?page=1']
    page = 1
    
    def parse(self, response):
        urls = response.xpath("//div[@class='main']/div[@class='row'][2]/div[@class='col-md-12']/div[@class='mod-best-girls']/div[@class='row']/article[@class='col-md-6']/div[@class='showcase-wrap']/div[@class='info-wrapper']/div[@class='line'][9]/a[@class='cometome']/@href").extract()
        urls_list = ProstituteModel.objects.filter(site_number=self.site_number).values_list('url')
        urls_list = [item[0] for item in urls_list]
        for url in urls:
            if url not in urls_list:
                yield scrapy.Request(url, callback=self.parse_item)

        if urls:
            self.page += 1
            next_page = re.sub(r'(?<=page=)\d+', str(self.page), response.url)
            yield scrapy.Request(next_page, callback=self.parse)

    def parse_item(self, response):
        self.logger.info('////start parsing'+response.url+'////')
        ladie = LadieItem()
        ladie['url'] = response.url
        ladie['site_number'] = self.site_number
        ladie['city'] = u'kyiv'

        info_table = response.selector.xpath("//div[@class='product-item']/div[@class='row']")

        ladie['name'] = info_table.xpath(".//span[@id='model_name']/h1/text()").extract_first()
        phone = info_table.xpath(".//div[@class='ico-phone']/text()").extract_first()
        ladie['phone'] = phone.strip() if phone else ''

        price = info_table.xpath(".//div[@class='pricing-model'][1]/a/text()").re(r'(\d+)\s(\w+)')
        if price and len(price) == 2:
            ladie['price_1'] = price[0]
            ladie['currency'] = price[1]
            price_2 = info_table.xpath(".//div[@class='pricing-model'][2]/a/text()").re(r'[\d]+')
            ladie['price_2'] = price_2[0] if price_2 else ''
            price_night = info_table.xpath(".//div[@class='pricing-model'][3]/a/text()").re(r'[\d]+')
            ladie['price_night'] = price_night[0] if price_night else ''

        ladie['age'] = info_table.xpath(".//div[@id='pane_tab1']/div[@class='info-wrapper'][1]/div[@class='line'][1]/a/text()").extract_first()
        ladie['height'] = info_table.xpath(".//div[@id='pane_tab1']/div[@class='info-wrapper'][1]/div[@class='line'][2]/a/text()").extract_first()
        bust = info_table.xpath(".//div[@id='pane_tab1']/div[@class='info-wrapper'][1]/div[@class='line'][3]/a/text()").re(r'[\d]')
        ladie['bust'] = bust[0] if bust else ''
        ladie['weight'] = info_table.xpath(".//div[@id='pane_tab1']/div[@class='info-wrapper'][1]/div[@class='line'][4]/a/text()").extract_first()


        proposes = info_table.xpath(".//ul[@class='uslugi-ul-anketa']/li[@class='uslugi-anketa']/a/text()").extract()
        proposes = [propose.strip() for propose in proposes]
        ladie['proposes'] = json.dumps(proposes)

        description = info_table.xpath(".//div[@class='info-wrapper'][3]/div[@class='info-models']").extract_first()
        ladie['description'] = re.sub(r'(\<(/?[^>]+)>)', '', description).strip() if description else ''


        image_urls = info_table.xpath("//div[@class='es-carousel']//img/@src").extract()
        ladie['image_urls'] = json.dumps([urlparse.urljoin(response.url, image_url) for image_url in image_urls])
        yield ladie
