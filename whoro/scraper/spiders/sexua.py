# -*- coding: utf-8 -*-
from ..items import LadieItem
import datetime, json, re, scrapy
from scrapy.spiders import CrawlSpider, Rule, Spider, SitemapSpider
from scrapy.linkextractors import LinkExtractor
import urlparse
from scrapy import Selector
from prostitute.models import ProstituteModel
from prostitute.models import CITIES


class SexUaSpider(Spider):
    name = 'sexua'
    allowed_domains = ['sex-ukraine.net']
    site_number = 8
    start_urls = ['http://sex-ukraine.net/']
    page = 1

    def parse(self, response):
        # i = 0
        urls = response.xpath("//div[@class='index_right']//div[@class='hearts']/span[@class='left']/a/@href").extract()
        urls_list = ProstituteModel.objects.filter(site_number=self.site_number).values_list('url')
        urls_list = [item[0] for item in urls_list]
        # self.logger.info('!!!!!!!!!!!!!!!!!!!!'+str(len(urls))+' count of urls')
        for url in urls:
            if url not in urls_list:
                yield scrapy.Request(url, callback=self.parse_item)
            # else:
            #     i += 1

        if not urls:
            cookie = response.xpath("//script").re(r'(?<=document.cookie=")[^;]+')
            cookie = cookie[0].split('=') if cookie else ''
            if cookie:
                yield scrapy.Request(response.url, cookies={cookie[0]: cookie[1]}, callback=self.parse, dont_filter=True)
        else:
            line = response.xpath("//div[@class='index_right']//div[@class='line']").extract_first()

            self.page += 1
            next_page = urlparse.urljoin(response.url, 'page-%s' % self.page)
            # next_page = response.xpath("//div[@class='index_right']/div[@class='pager']/div[@class='right']/a/@href").extract_first()
            self.logger.info('1!!!!!!!!!!!!!!!!!!!!'+str(next_page)+' next page')
            if line is not None and self.page < 60:
                yield scrapy.Request(next_page, callback=self.parse)

    def parse_item(self, response):
        if not response.xpath("//div[@class='body']"):
            cookie = response.xpath("//script").re(r'(?<=document.cookie=")[^;]+')
            cookie = cookie[0].split('=') if cookie else ''
            if cookie:
                yield scrapy.Request(response.url, cookies={cookie[0]: cookie[1]}, callback=self.parse_item, dont_filter=True)
        else:
            self.logger.info('////start parsing'+response.url+'////')
            ladie = LadieItem()
            ladie['url'] = response.url
            ladie['site_number'] = self.site_number

            city = re.search(r'(?<=prostitutki-)\w+', response.url).group()
            if city:
                for key, value in CITIES.items():
                    if city.find(key) != -1:
                        ladie['city'] = value
                        break
                else:
                    print city
                    raise Exception('////////Not found city/////////////////////////')

            info_table = response.selector.xpath("//div[@class='body']/div[contains(concat(' ', normalize-space(@class), ' '), ' index ')]")

            ladie['name'] = info_table.xpath(".//div[@class='form_top_layout']/div[@class='all'][1]/span[@class='right']/b/text()").extract_first()
            ladie['phone'] = info_table.xpath(".//div[@class='form_top_layout']/div[@class='all'][2]/span[@id='phone']/div[@class='phone_ajax']/span/text()").extract_first()

            price = info_table.xpath(".//div[@class='form_top_layout']/div[@class='all'][3]/span[@class='left_small'][1]/b/text()").re(r'(\d+)\s(\w+)')
            if price and len(price) == 2:
                ladie['price_1'] = price[0]
                ladie['currency'] = price[1]
                price_2 = info_table.xpath(".//div[@class='form_top_layout']/div[@class='all'][3]/span[@class='left_small'][2]/text()").re(r'[\d]+')
                ladie['price_2'] = price_2[1] if len(price_2) == 2 else ''
                price_night = info_table.xpath(".//div[@class='form_top_layout']/div[@class='all'][3]/span[@class='left_small'][3]/text()").re(r'[\d]+')
                ladie['price_night'] = price_night[0] if price_night else ''


            ladie['age'] = info_table.xpath(".//div[@class='all'][2]/span[@class='right']/b/text()").extract_first()
            ladie['height'] = info_table.xpath(".//div[@class='all'][3]/span[@class='right']/b/text()").extract_first()
            ladie['bust'] = info_table.xpath(".//div[@class='all'][4]/span[@class='right']/b/text()").extract_first()
            ladie['weight'] = info_table.xpath(".//div[@class='all'][5]/span[@class='right']/b/text()").extract_first()

            district = info_table.xpath(".//div[@class='all']/span[@class='right']/i").extract_first()
            ladie['district'] = re.sub(r'(\<(/?[^>]+)>)', '', district) if district else ''

            ladie['description'] = info_table.xpath(".//div[@class='all'][8]/span[1]/text()").extract_first()

            proposes = info_table.xpath(".//div[@class='dotted'][1]/div[@class='all']/span[1]/div[@class='services']/ul/li[@class='tick']/text()").extract()
            proposes = [propose.strip() for propose in proposes]
            ladie['proposes'] = json.dumps(proposes)

            image_urls = info_table.xpath("./div[@class='form_left']//img/@src").extract()
            image_urls = list(set(image_urls))
            ladie['image_urls'] = json.dumps([urlparse.urljoin(response.url, image_url) for image_url in image_urls])
            yield ladie
