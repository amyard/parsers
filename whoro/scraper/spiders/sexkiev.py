# -*- coding: utf-8 -*-
from ..items import LadieItem
import datetime, json, re, scrapy
from scrapy.spiders import CrawlSpider, Rule, Spider, SitemapSpider
from scrapy.linkextractors import LinkExtractor
import urlparse
from scrapy import Selector
import xml.etree.ElementTree as ET
import os
from prostitute.models import ProstituteModel

# UnicodeDecodeError: 'ascii' codec can't decode byte 0xd0 in position 0: ordinal not in range(128)
import sys
reload(sys)  
sys.setdefaultencoding('utf8')



def clean_name(name):
    name = name.replace('_', '') if '_' in name else name
    name = name.replace(')', '') if ')' in name else name
    name = name.replace('!', '') if '!' in name else name
    name = name.replace('*', '') if '*' in name else name
    name = name.replace(' - ', '') if ' - ' in name else name
    name = name.replace('↔', '') if '↔' in name else name
    
    name = name.replace('-ФОТО-', '') if '-ФОТО-' in name else name
    name = name.replace('ФОТО', '') if 'ФОТО' in name else name
    name = name.replace('Фото', '') if 'Фото' in name else name
    name = name.split('100%')[0] if '100%' in name else name
    return name.strip()


class SexKievSpider(Spider):
    name = 'sexkiev'
    allowed_domains = ['sexkiev.net']
    site_number = 7
    start_urls = ['http://sexkiev.net/']
    page = 1

    def parse(self, response):

        # это id 
        urls = response.selector.xpath("//div[@class='line']/div[@class='hearts']/span[@class='left']/a/@href").extract()
        urls_list = ProstituteModel.objects.filter(site_number=self.site_number).values_list('url')
        urls_list = [item[0] for item in urls_list]


        for url in urls:
            url = urlparse.urljoin(response.url, url)
            if url not in urls_list:
                yield scrapy.Request(url, callback=self.parse_item)

        if urls:
            self.page += 1
            next_page = urlparse.urljoin(response.url, 'page-%s' % self.page)
            yield scrapy.Request(next_page, callback=self.parse)




        # # for sitemap
        # root = ET.fromstring(response.text.encode('utf-8'))
        # urls_list = ProstituteModel.objects.filter(site_number=self.site_number).values_list('url')
        # urls_list = [item[0] for item in urls_list]
        # for href in root:
        #     url = href[0].text
        #     if url.find('sexkiev.net/prostitutki') == -1:
        #         continue
        #     if not url in urls_list:
        #         yield scrapy.Request(url, callback=self.parse_item)

    def parse_item(self, response):
        self.logger.info('////start parsing'+response.url+'////')
        ladie = LadieItem()
        ladie['url'] = response.url
        ladie['site_number'] = self.site_number
        ladie['city'] = u'kyiv'

        info_table = response.selector.xpath(".//div[@class='index'][1]")
        dirty_name = info_table.xpath(".//div[@class='form_top_layout']/div[@class='all'][1]/span[@class='right']/b/text()").extract_first()
        ladie['name'] = clean_name(dirty_name)

        ladie['phone'] = info_table.xpath(".//div[@class='form_top_layout']/div[@class='all'][2]/span[@id='phone']/div[@class='phone_ajax']/span/text()").extract_first()

        price = info_table.xpath(".//div[@class='form_top_layout']/div[@class='all'][3]/span[@class='left_small'][1]/a/text()").re(r'(\d+)\s(\w+)')
        if price and len(price) == 2:
            ladie['price_1'] = price[0]
            ladie['currency'] = price[1]
            price_2 = info_table.xpath(".//div[@class='form_top_layout']/div[@class='all'][3]/span[@class='left_small'][2]/text()").re(r'[\d]+')
            ladie['price_2'] = price_2[1] if len(price_2) == 2 else ''
            price_night = info_table.xpath(".//div[@class='form_top_layout']/div[@class='all'][3]/span[@class='left_small'][3]/text()").re(r'[\d]+')
            ladie['price_night'] = price_night[0] if price_night else ''

        ladie['age'] = info_table.xpath(".//div[@class='all'][2]/span[@class='right']/a/text()").extract_first()
        ladie['height'] = info_table.xpath(".//div[@class='all'][3]/span[@class='right']/a/text()").extract_first()
        ladie['bust'] = info_table.xpath(".//div[@class='all'][4]/span[@class='right']/a/text()").extract_first()
        ladie['weight'] = info_table.xpath(".//div[@class='all'][5]/span[@class='right']/a/text()").extract_first()

        district = info_table.xpath(".//div[@class='all']/span[@class='right']/i").extract_first()
        ladie['district'] = re.sub(r'(\<(/?[^>]+)>)', '', district) if district else ''

        ladie['description'] = info_table.xpath(".//div[@class='all'][8]/span[1]/text()").extract_first()

        proposes = info_table.xpath(".//div[@class='dotted'][1]/div[@class='all']/span[1]/div[@class='services']/ul/li[@class='tick']/a/text()").extract()
        proposes = [propose.strip() for propose in proposes]
        ladie['proposes'] = json.dumps(proposes)

        image_urls = info_table.xpath("./div[@class='form_left']//img/@src").extract()
        image_urls = list(set(image_urls))
        ladie['image_urls'] = json.dumps([urlparse.urljoin(response.url, image_url) for image_url in image_urls])
        return ladie

