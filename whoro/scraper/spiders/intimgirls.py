# -*- coding: utf-8 -*-
from ..items import LadieItem
import datetime, json, re, scrapy
from scrapy.spiders import CrawlSpider, Rule, Spider, SitemapSpider
from scrapy.linkextractors import LinkExtractor
import urlparse
from scrapy import Selector
from prostitute.models import ProstituteModel
from prostitute.models import CITIES


class IntimGirlsSpider(Spider):
    name = 'intimgirls'
    allowed_domains = ['intimgirls.net']
    site_number = 10
    start_urls = ['https://intimgirls.net',
                  'https://intimgirls.net/dnepropetrovsk',
                  'https://intimgirls.net/zaporozhe',
                  'https://intimgirls.net/lvov',
                  'https://intimgirls.net/nikolaev',
                  'https://intimgirls.net/odessa',
                  'https://intimgirls.net/kharkov',
                  ]

    def parse(self, response):
        urls = response.xpath("//div[@class='main-items']//h3/a/@href").extract()
        urls_list = ProstituteModel.objects.filter(site_number=self.site_number).values_list('url')
        urls_list = [item[0] for item in urls_list]
        for url in urls:
            url = urlparse.urljoin(response.url, url)
            if url not in urls_list:
                yield scrapy.Request(url, callback=self.parse_item)
        page = response.xpath("//div[@class='main']/div[@class='paginator']/ul/li[last()]/a")
        if page.xpath('./nobr'):
            next_page = page.xpath('./@href').extract_first()
            if next_page is not None:
                next_page = urlparse.urljoin(response.url, next_page)
                yield scrapy.Request(next_page, callback=self.parse)

    def parse_item(self, response):
        self.logger.info('////start parsing'+response.url+'////')
        ladie = LadieItem()
        ladie['url'] = response.url
        ladie['site_number'] = self.site_number
        city = re.search(r'(?<=intimgirls.net/)\w+', response.url).group()
        if city:
            for key, value in CITIES.items():
                if city.find(key) != -1:
                    ladie['city'] = value
                    break
            else:
                ladie['city'] = u'kyiv'

        info_table = response.xpath("//div[@class='girl-full-main']")

        ladie['name'] = info_table.xpath("./h3/text()").extract_first()

        age = info_table.xpath("./ul[1]/li[1]/b/text()").re(r'[\d]+')
        ladie['age'] = age[0] if age else ''
        height = info_table.xpath("./ul[1]/li[2]/b/text()").re(r'[\d]+')
        ladie['height'] = height[0] if height else ''
        weight = info_table.xpath("./ul[1]/li[3]/b/text()").re(r'[\d]+')
        ladie['weight'] = weight[0] if weight else ''
        bust = info_table.xpath("./ul[1]/li[4]/b/text()").re(r'[\d]+')
        ladie['bust'] = bust[0] if bust else ''

        ladie['district'] = info_table.xpath("./ul[1]/li[6]/b/text()").extract_first()
        
        price = info_table.xpath("./ul[2]/li[1]/b/text()").extract_first()
        price = re.findall(r'(\d+)(\D+)', price.replace(' ', ''))

        if price and len(price) == 2:
            ladie['price_1'] = price[0]
            ladie['currency'] = price[1]
            price_2 = info_table.xpath("./ul[2]/li[2]/b/text()").re(r'[\s\d]+')
            ladie['price_2'] = price_2[0].replace(' ', '') if price_2 else ''
            price_night = info_table.xpath("./ul[2]/li[3]/b/text()").re(r'[\s\d]+')
            ladie['price_night'] = price_night[0].replace(' ', '') if price_night else ''
        description = info_table.xpath("./ul[3]/li").extract()
        ladie['description'] = re.sub(r'(\<(/?[^>]+)>)', '', '. '.join(description)) if description else ''
        proposes = info_table.xpath("./div[@class='girl-trud']/ul/li/a/text()").extract()
        proposes = [propose.strip() for propose in proposes]
        ladie['proposes'] = json.dumps(proposes)

        image_urls = response.xpath("//div[@class='girl-full-image']/a/img/@src").extract()
        image_urls = list(set(image_urls))
        ladie['image_urls'] = json.dumps([urlparse.urljoin(response.url, image_url) for image_url in image_urls])
        yield ladie






