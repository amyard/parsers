# -*- coding: utf-8 -*-

from ..items import LadieItem
import datetime, json, re, scrapy
from scrapy.spiders import CrawlSpider, Rule, Spider, SitemapSpider
from scrapy.linkextractors import LinkExtractor
import urlparse
from scrapy import Selector
import os
from prostitute.models import ProstituteModel


class MinuetSpider(Spider):
    name = 'minuet'
    allowed_domains = ['minuet.biz']
    site_number = 4

    start_urls = [
    'http://minuet.biz/main',
    'http://minuet.biz/main/dnepropetrovsk',
    'http://minuet.biz/main/donetsk',
    'http://minuet.biz/main/nikolaev',
    'http://minuet.biz/main/odessa',
    'http://minuet.biz/main/kharkov',
    'http://minuet.biz/main/zaporoje',
    ]

    def parse(self, response):
        urls = response.xpath("//div[@class='main_girls_div']/a/@href").extract()
        urls_list = ProstituteModel.objects.filter(site_number=self.site_number).values_list('url')
        urls_list = [item[0] for item in urls_list]
        city = response.url.split('/')[4] if (len(response.url.split('/')) == 5) else 'kyiv'
        for url in urls:
            url = urlparse.urljoin(response.url, url)
            if not (url in urls_list):
                yield scrapy.Request(url, callback=self.parse_item, meta={'city': city})

    def parse_item(self, response):
        self.logger.info('////start parsing////')
        ladie = LadieItem()
        ladie['url'] = response.url
        ladie['site_number'] = self.site_number
        response.selector.xpath("//div[@class='wrapper']/div[@class='breadcrumbs']/span[1]/a/text()")
        ladie['city'] = response.meta['city']
        # ladie['city'] = response.selector.xpath("//div[@class='wrapper']/div[@class='breadcrumbs']/span[1]/a/text()").re(r'\s.+')[0].strip()

        info_table = response.selector.xpath("//div[@class='one_girl_block rounded_block_1']")

        ladie['name'] = info_table.xpath(".//span[@class='one_girl_name pull-left']/text()").extract_first()


        age = info_table.xpath(".//table[@class='girl_params_tbl']//tr/td[1]/ul[@class='girl_params']/li[1]/a/text()").re(r'\d+')
        ladie['age'] = age[0] if age else ''
        height = info_table.xpath(".//table[@class='girl_params_tbl']//tr/td[1]/ul[@class='girl_params']/li[2]/a/text()").re(r'\d+')
        ladie['height'] = height[0] if height else ''
        bust = info_table.xpath(".//table[@class='girl_params_tbl']//tr/td[1]/ul[@class='girl_params']/li[3]/a/text()").re(r'\d+')
        ladie['bust'] = bust[0] if bust else ''

        # need filter over words "Район:" "Ближайшее метро:"
        district = info_table.xpath(".//table[@class='girl_params_tbl']//tr/td[2]/ul[@class='girl_params']/li[2]/text()").re(r'[\s\w]+')
        ladie['district'] = district[0] if district else ''
        underground = info_table.xpath(".//table[@class='girl_params_tbl']//tr/td[3]/ul[@class='girl_params']/li[2]/text()").re(r'[\s\w]+')
        ladie['underground'] = underground[0] if underground else ''

        price_1 = info_table.xpath(".//div[@class='info_block'][2]/ul[@class='prices']/li[1]/span[@class='price rounded_block_4']/text()").re(r'[\d]+')
        ladie['price_1'] = price_1[0] if price_1 else ''
        currency = info_table.xpath(".//div[@class='info_block'][2]/ul[@class='prices']/li[1]/span[@class='price rounded_block_4']/span/text()").re(r'[\w]+')
        ladie['currency'] = currency[0] if currency else ''
        price_2 = info_table.xpath(".//div[@class='info_block'][2]/ul[@class='prices']/li[2]/span[@class='price rounded_block_4']/text()").re(r'[\d]+')
        ladie['price_2'] = price_2[0] if price_2 else ''
        price_night = info_table.xpath(".//div[@class='info_block'][2]/ul[@class='prices']/li[3]/span[@class='price rounded_block_4']/text()").re(r'[\d]+')
        ladie['price_night'] = price_night[0] if price_night else ''
        
        ladie['phone'] = info_table.xpath(".//div[@class='info_block'][3]/span[@class='one_girl_phone rounded_block_5']/text()").extract_first()


        proposes = info_table.xpath(".//div[@class='info_block'][4]/ul[@class='services']/li/text()").extract()
        proposes = [propose.strip() for propose in proposes]
        ladie['proposes'] = json.dumps(proposes)

        ladie['description'] = info_table.xpath(".//div[@class='info_block'][4]/span[@class='girl_about']/text()").extract_first().strip()

        image_urls = info_table.xpath(".//div[@class='girl_photos']/img[@class='girl_photo']/@src").extract()
        ladie['image_urls'] = json.dumps([urlparse.urljoin(response.url, image_url) for image_url in image_urls])
        return ladie