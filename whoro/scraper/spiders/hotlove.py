# -*- coding: utf-8 -*-
from ..items import LadieItem
import datetime, json, re, scrapy
from scrapy.spiders import CrawlSpider, Rule, Spider, SitemapSpider
from scrapy.linkextractors import LinkExtractor
import urlparse
from scrapy import Selector
import xml.etree.ElementTree as ET
import os
from prostitute.models import ProstituteModel
from prostitute.models import Cityes_hot_love


class HotLoveSpider(Spider):
    name = 'hotlove'
    allowed_domains = ['hot-love.org']
    site_number = 12
    start_urls = ['http://hot-love.org/sitemap.xml']

    def parse(self, response):
        root = ET.fromstring(response.text.encode('utf-8'))
        urls_list = ProstituteModel.objects.filter(site_number=self.site_number).values_list('url')
        urls_list = [item[0] for item in urls_list]
        for href in root:
            url = href[0].text
            if url.find('girl/view') == -1:
                continue
            if url not in urls_list:
                yield scrapy.Request(url, callback=self.parse_item)

    def parse_item(self, response):
        self.logger.info('////start parsing'+response.url+'////')
        ladie = LadieItem()
        ladie['url'] = response.url
        ladie['site_number'] = self.site_number

        info_table = response.selector.xpath("//div[@id='main-content']/div[@id='sidebar']")

        ladie['name'] = info_table.xpath(".//h4[1]/span[@class='anket-name']/text()").extract_first()
        phone = info_table.xpath(".//h4[1]/span[@class='anket-name right']/text()").extract_first()
        ladie['phone'] = phone.strip() if phone else ''

        info = info_table.xpath(".//div[@class='textwidget anket-description']")
        ladie['age'] = info.xpath("./p[1]/span/text()").extract_first()
        ladie['height'] = info.xpath("./p[2]/span/text()").extract_first()
        ladie['weight'] = info.xpath("./p[3]/span/text()").extract_first()
        ladie['bust'] = info.xpath("./p[4]/span/text()").extract_first()
        city = info.xpath("./p[5]/span/text()").extract_first()
        if city in Cityes_hot_love:
            ladie['city'] = Cityes_hot_love[city]
        else:
            ladie['city'] = u'other {}'.format(city)
            # raise Exception('Not found any city #####################'+city)
        
        ladie['price_1'] = info.xpath("./p[6]/span/text()").extract_first()
        ladie['currency'] = 'грн'
        ladie['price_2'] = info.xpath("./p[7]/span/text()").extract_first()
        ladie['price_night'] = info.xpath("./p[8]/span/text()").extract_first()

        proposes = info_table.xpath(".//div[@class='textwidget']/a/text()").extract()
        ladie['proposes'] = json.dumps(proposes)

        image_urls = response.selector.xpath("//div[@id='main-content']//li[@class='slide']/img/@src").extract()
        image_urls = list(set(image_urls))
        ladie['image_urls'] = json.dumps(image_urls)

        description = response.xpath("//article[@class='entry clearfix']/text()").extract_first()
        ladie['description'] = description.strip() if description else ''
        return ladie




