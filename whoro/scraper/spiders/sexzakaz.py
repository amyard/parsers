# -*- coding: utf-8 -*-

from ..items import LadieItem
import datetime, json, re, scrapy
from scrapy.spiders import CrawlSpider, Rule, Spider, SitemapSpider
from scrapy.linkextractors import LinkExtractor
import urlparse
from scrapy import Selector
import os
from prostitute.models import ProstituteModel


class SexzakazSpider(Spider):
    name = 'sexzakaz'
    allowed_domains = ['sexzakaz.net']
    site_number = 5
    start_urls = [
    'http://sexzakaz.net/page-1',
    ]

    def parse(self, response):
        urls = response.xpath(".//div[@class='body']/div[@class='index']/div[@class='index_right']//div[@class='left']/a/@href").extract()
        urls_list = ProstituteModel.objects.filter(site_number=self.site_number).values_list('url')
        urls_list = [item[0] for item in urls_list]
        for url in urls:
            url = urlparse.urljoin(response.url, url)
            if not (url in urls_list):
                yield scrapy.Request(url, callback=self.parse_item)
        # print urls
        if not urls:
            cookie = response.xpath("//script").re(r'(?<=document.cookie=")[^;]+')
            cookie = cookie[0].split('=') if cookie else ''
            if cookie:
                yield scrapy.Request(response.url, cookies={cookie[0]: cookie[1]}, callback=self.parse, dont_filter=True)
        next_page = response.xpath("//div[@class='right']/a[@rel='nofollow']/@href").extract_first()
        next_page = urlparse.urljoin(response.url, next_page)
        # print next_page
        if next_page is not None:
            yield scrapy.Request(next_page, callback=self.parse)

    def parse_item(self, response):
        self.logger.info('////start parsing////')
        ladie = LadieItem()
        ladie['url'] = response.url
        ladie['site_number'] = self.site_number
        ladie['city'] = u'kyiv'
        info_table = response.selector.xpath(".//div[@class='index']/div[@class='index_right']")

        ladie['name'] = info_table.xpath(".//div[@class='form_right']/div[@class='form_top_layout']/div[@class='all'][1]/b/text()").extract_first()

        ladie['phone'] = info_table.xpath(".//div[@class='form_right']/div[@class='form_top_layout']/div[@class='phone_ajax all']/span/text()").extract_first()

        price_1 = info_table.xpath(".//div[@class='form_right']/div[@class='form_top_layout']/div[@class='all'][2]/span[@class='left_small'][1]/a/text()").re(r'[\d]+')
        ladie['price_1'] = price_1[0] if price_1 else ''
        currency = info_table.xpath(".//div[@class='form_right']/div[@class='form_top_layout']/div[@class='all'][2]/span[@class='left_small'][1]/a/text()").re(r'[\w]+')
        ladie['currency'] = currency[0] if currency else ''
        price_2 = info_table.xpath(".//div[@class='form_right']/div[@class='form_top_layout']/div[@class='all'][2]/span[@class='left_small'][2]/text()").re(r'[\d]+')
        ladie['price_2'] = price_2[1] if len(price_2)==2 else ''
        price_night = info_table.xpath(".//div[@class='form_right']/div[@class='form_top_layout']/div[@class='all'][2]/span[@class='left_small'][3]/text()").re(r'[\d]+')
        ladie['price_night'] = price_night[0] if price_night else ''

        ladie['age'] = info_table.xpath(".//div[@class='form_right']/div[@class='all'][2]/span[@class='right']/a/text()").extract_first()
        ladie['height'] = info_table.xpath(".//div[@class='form_right']/div[@class='all'][3]/span[@class='right']/a/text()").extract_first()
        ladie['bust'] = info_table.xpath(".//div[@class='form_right']/div[@class='all'][4]/span[@class='right']/a/text()").extract_first()
        ladie['weight'] = info_table.xpath(".//div[@class='form_right']/div[@class='all'][5]/span[@class='right']/a/text()").extract_first()

        district = info_table.xpath(".//table[@class='girl_params_tbl']//tr/td[2]/ul[@class='girl_params']/li[2]/text()").re(r'[\s\w]+')
        ladie['district'] = district[0] if district else ''
        
        ladie['description'] = info_table.xpath(".//div[@class='form_right']/div[@class='all'][8]/span[1]/text()").extract_first()

        proposes = info_table.xpath(".//div[@class='form_right']/div[@class='dotted'][1]/div[@class='all']/span[1]/div[@class='services']/ul/li[@class='tick']/a/text()").extract()
        proposes = [propose.strip() for propose in proposes]
        ladie['proposes'] = json.dumps(proposes)

        image_urls = info_table.xpath(".//div[@class='form_left']/div[@class='form_left_bg'][1]/a/img/@src").extract()
        ladie['image_urls'] = json.dumps([urlparse.urljoin(response.url, image_url) for image_url in image_urls])
        return ladie