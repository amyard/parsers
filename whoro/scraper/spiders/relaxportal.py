# -*- coding: utf-8 -*-

from ..items import LadieItem
import datetime, json, re, scrapy
from scrapy.spiders import CrawlSpider, Rule, Spider, SitemapSpider
from scrapy.linkextractors import LinkExtractor
import urlparse
from scrapy import Selector
from prostitute.models import ProstituteModel
from lxml import etree
import requests


# UnicodeDecodeError: 'ascii' codec can't decode byte 0xd0 in position 0: ordinal not in range(128)
import sys
reload(sys)  
sys.setdefaultencoding('cp1251')


class RelaxportalSpider(Spider):
    name = 'relaxportal'
    allowed_domains = ['relaxportal.biz']
    site_number = 2
    start_urls = [
    'https://vinnitsa.relaxportal.biz/sitemap/',
    'https://dnepropetrovsk.relaxportal.biz/sitemap/',
    'https://donetsk.relaxportal.biz/sitemap/',
    'https://zhitomir.relaxportal.biz/sitemap/',
    'https://zaporizhzhya.relaxportal.biz/sitemap/',
    'https://ivano-frankivsk.relaxportal.biz/sitemap/',
    'https://relaxportal.biz/sitemap/', #kiev
    'https://krivoy-rog.relaxportal.biz/sitemap/',
    'https://lviv.relaxportal.biz/sitemap/',
    'https://nikolaev.relaxportal.biz/sitemap/',
    'https://odessa.relaxportal.biz/sitemap/',
    'https://poltava.relaxportal.biz/sitemap/',
    'https://ternopil.relaxportal.biz/sitemap/',
    'https://kharkov.relaxportal.biz/sitemap/',
    'https://kherson.relaxportal.biz/sitemap/',
    'https://khmelnitsky.relaxportal.biz/sitemap/',
    'https://cherkasy.relaxportal.biz/sitemap/',
    'https://chernihiv.relaxportal.biz/sitemap/',
    'https://chernivtsi.relaxportal.biz/sitemap/',
    ]

    def parse(self, response):
        urls = response.xpath("//a").re(r'[\w/:-]+.relaxportal.biz/\d+/')
        sitemap = scrapy.selector.XmlXPathSelector(response)
        sitemap.register_namespace( 'ns', 'http://www.sitemaps.org/schemas/sitemap/0.9' )
        locsList = sitemap.select('//ns:loc/text()').extract()


        urls = []
        for url in locsList:
            lalal = re.findall('\W*relaxportal.biz/(\d+)', url)
            if lalal:
                urls.append(url)

        urls_list = ProstituteModel.objects.filter(site_number=self.site_number).values_list('url')
        urls_list = [item[0] for item in urls_list]
        match = re.search('[-\w]+(?=\.relaxportal\.biz)', response.url)
        city = match.group(0) if match else 'kyiv'
        for url in urls:
            if not url in urls_list:
                yield scrapy.Request(url, callback=self.parse_item, meta={'city': city})

    def parse_item(self, response):
        self.logger.info('////start parsing'+response.url+'////')
        ladie = LadieItem()
        ladie['url'] = response.url
        ladie['site_number'] = self.site_number

        info_table = response.selector.xpath("//div[@class='container content']//div[@class='col-md-8 col-md-push-4   col-lg-9 col-lg-push-3  content-right']")
        ladie['name'] = response.selector.xpath("//div[contains(@class,'name')]//text()")[2].extract()

        ladie['city'] = response.meta['city']

        ladie['district'] = response.selector.xpath("//div[contains(@class,'bl-content-3 col-xs-12')]//ul[contains(@class, 'table')][2]//li[2]//div[2]//text()").extract_first()
        ladie['underground'] = response.selector.xpath("//div[contains(@class,'bl-content-3 col-xs-12')]//ul[contains(@class, 'table')][2]//li[3]//div[2]//text()").extract_first()
        
        ladie['phone'] = response.selector.xpath("//div[contains(@class,'number')]//a/@href").extract_first()

        age = response.selector.xpath("//div[contains(@class,'bl-content-3 col-xs-12')]//ul[contains(@class, 'table')][1]//li[1]//div[2]//a/strong/text()").extract_first()
        ladie['age'] = re.findall('\d+', age)[0]

        height = response.selector.xpath("//div[contains(@class,'bl-content-3 col-xs-12')]//ul[contains(@class, 'table')][1]//li[2]//div[2]//a/strong/text()").extract_first()
        ladie['height'] = re.findall('\d+', height)[0]

        weight = response.selector.xpath("//div[contains(@class,'bl-content-3 col-xs-12')]//ul[contains(@class, 'table')][1]//li[3]//div[2]//a/strong/text()").extract_first()
        ladie['weight'] = re.findall('\d+', weight)[0]


        ladie['bust'] = int(response.selector.xpath("//div[contains(@class,'bl-content-3 col-xs-12')]//ul[contains(@class, 'table')][1]//li[4]//div[2]//a/strong/text()").extract_first())
        
        price = response.selector.xpath(".//div[@class='bl-content-3 col-xs-12 profile']/div[@class='col-sm-7 left-block']/div[@class='row price-block']/div[@class='col-lg-6 left-bl']/ul[@class='table']/li[1]/div[@class='price-value']/a/text()").re(r'(\d+).(\w+)')
        # price = response.selector.xpath(".//div[@class='bl-content-3 col-xs-12 profile']/div[@class='col-sm-7 left-block']/div[@class='row price-block']/div[@class='col-lg-6 left-bl']/ul[@class='table']/li[1]/div[@class='price-value']/a/text()").re(r'(\d+).(\w+)')

        if price and len(price) == 2:
            ladie['price_1'] = price[0]
            ladie['currency'] = price[1]
        else:
            price  = response.selector.xpath(".//div[@class='bl-content-3 col-xs-12 profile']/div[@class='col-sm-7 left-block']/div[@class='row price-block']/div[@class='col-lg-6 right-bl']/ul[@class='table']/li[1]/div[@class='price-value']/a/text()").re(r'(\d+).(\w+)')
            ladie['price_1'] = price[0] if price else ''
            ladie['currency'] = price[1]

        price_2 = response.selector.xpath(".//div[@class='bl-content-3 col-xs-12 profile']/div[@class='col-sm-7 left-block']/div[@class='row price-block']/div[@class='col-lg-6 left-bl']/ul[@class='table']/li[2]/div[@class='price-value']/text()").re(r'(\d+)')
        if price_2:
            ladie['price_2'] = price_2[0]
        else:
            price_2 = response.selector.xpath(".//div[@class='bl-content-3 col-xs-12 profile']/div[@class='col-sm-7 right-block']/div[@class='row price-block']/div[@class='col-lg-6 left-bl']/ul[@class='table']/li[2]/div[@class='price-value']/text()").re(r'(\d+)')
            ladie['price_2'] = price_2[0] if price_2 else ''

        price_night = response.selector.xpath(".//div[@class='bl-content-3 col-xs-12 profile']/div[@class='col-sm-7 left-block']/div[@class='row price-block']/div[@class='col-lg-6 left-bl']/ul[@class='table']/li[3]/div/text()").re(r'(\d+).(\w+)')
        if price_night:
            ladie['price_night'] = price_night[0]
        else:
            price_night = response.selector.xpath(".//div[@class='bl-content-3 col-xs-12 profile']/div[@class='col-sm-7 right-block']/div[@class='row price-block']/div[@class='col-lg-6 left-bl']/ul[@class='table']/li[3]/div/text()").re(r'(\d+).(\w+)')
            ladie['price_night'] = price_night[0] if price_night else ''

        proposes = response.selector.xpath(".//div[@class='row services-block']/div/div[@class='block']/ul/li[not(contains(@class, 'disabled'))]/a/text()").extract()
        ladie['proposes'] = json.dumps(proposes)

        ladie['description'] = response.selector.xpath(".//div[@class='bl-content-3 col-xs-12 profile']/div[@class='col-sm-7 left-block']/div[@class='table']/div/text()").extract_first()

        go_out = response.selector.xpath(".//div[@class='col-sm-7 left-block']/ul[@class='table'][3]/li[1]/div/strong/span/text()").extract()
        languages = response.selector.xpath(".//div[@class='col-sm-7 left-block']/ul[@class='table'][3]/li[2]/div/strong/span/text()").extract()
        # add different
        ladie['different'] = json.dumps({'go_out': go_out, 'languages': languages})
        image_urls = response.selector.xpath(".//div[@class='visible-xs photos-block-mob']/img[@class='img-responsive']/@src").extract()
        ladie['image_urls'] = json.dumps(image_urls)
        return ladie
