# -*- coding: utf-8 -*-
from ..items import LadieItem
import datetime, json, re, scrapy
from scrapy.spiders import CrawlSpider, Rule, Spider, SitemapSpider
from scrapy.linkextractors import LinkExtractor
import urlparse
from scrapy import Selector
from prostitute.models import ProstituteModel


class EscortKievSpider(Spider):
    name = 'escortkiev'
    allowed_domains = ['escortkiev.net']
    site_number = 9
    start_urls = ['http://escortkiev.net']
    page = 1

    def parse(self, response):
        urls = response.xpath("//div[@class='index_right']//div[@class='hearts']/span[@class='left']/a/@href").extract()
        urls_list = ProstituteModel.objects.filter(site_number=self.site_number).values_list('url')
        urls_list = [item[0] for item in urls_list]

        for url in urls:
            if url not in urls_list:
                yield scrapy.Request(url, callback=self.parse_item)
        
        if urls:
            self.page += 1
            next_page = urlparse.urljoin(response.url, 'page-%s' % self.page)
            yield scrapy.Request(next_page, callback=self.parse)

    def parse_item(self, response):
        self.logger.info('////start parsing'+response.url+'////')
        ladie = LadieItem()
        ladie['url'] = response.url
        ladie['site_number'] = self.site_number
        ladie['city'] = u'kyiv'


        info_table = response.selector.xpath("//div[@class='body']/div[@class='index'][1]/div[contains(concat(' ', normalize-space(@class), ' '), ' form_right ')]")

        ladie['name'] = info_table.xpath(".//div[@class='phone_ajax all']/b/text()").extract_first()
        ladie['phone'] = info_table.xpath(".//div[@class='phone_ajax all']/span/text()").extract_first()

        price = info_table.xpath(".//div[@class='form_top_layout']/table//tr[2]/td[1]/text()").re(r'(\d+)\s(\w+)')
        if price and len(price) == 2:
            ladie['price_1'] = price[0]
            ladie['currency'] = price[1]
            price_2 = info_table.xpath(".//div[@class='form_top_layout']/table//tr[2]/td[2]/text()").re(r'[\d]+')
            ladie['price_2'] = price_2[0] if price_2 else ''
            price_night = info_table.xpath(".//div[@class='form_top_layout']/table//tr[2]/td[3]/text()").re(r'[\d]+')
            ladie['price_night'] = price_night[0] if price_night else ''

        ladie['age'] = info_table.xpath(".//div[@class='all'][2]/span[@class='right']/a/text()").extract_first()
        ladie['height'] = info_table.xpath(".//div[@class='all'][3]/span[@class='right']/a/text()").extract_first()
        ladie['bust'] = info_table.xpath(".//div[@class='all'][4]/span[@class='right']/a/text()").extract_first()
        ladie['weight'] = info_table.xpath(".//div[@class='all'][5]/span[@class='right']/a/text()").extract_first()

        district = info_table.xpath(".//div[@class='all'][7]/span[@class='right']/i").extract_first()
        ladie['district'] = re.sub(r'(\<(/?[^>]+)>)', '', district) if district else ''

        ladie['description'] = info_table.xpath(".//div[@class='all'][9]/span[1]/text()").extract_first()

        proposes = info_table.xpath(".//div[@class='dotted'][1]/div[@class='all']/span[1]/div[@class='services']/ul/li[@class='tick']/a/text()").extract()
        proposes = [propose.strip() for propose in proposes]
        ladie['proposes'] = json.dumps(proposes)


        image_urls = response.xpath("//div[@class='form_left']//img/@src").extract()
        image_urls = list(set(image_urls))
        ladie['image_urls'] = json.dumps([urlparse.urljoin(response.url, image_url) for image_url in image_urls])
        yield ladie

