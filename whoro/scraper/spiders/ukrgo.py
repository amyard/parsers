# -*- coding: utf-8 -*-

from ..items import LadieItem
import datetime, json, re, scrapy
from scrapy.spiders import CrawlSpider, Rule, Spider, SitemapSpider
from scrapy.linkextractors import LinkExtractor
import urlparse
from scrapy import Selector
from prostitute.models import ProstituteModel


class UkrgoSpider(Spider):
    site_number = 3
    name = 'ukrgo'
    allowed_domains = ['ukrgo.com']
    start_urls = [
    'http://kiev.ukrgo.com/search.php?search=%C8%ED%F2%E8%EC%20%F3%F1%EB%F3%E3%E8&id_region=56&id_section=9&id_subsection=146&page=1&',
    ]

    def parse(self, response):
        urls = response.xpath("//table[3]//tr[2]/td[1]/div[5]/div//td/h3/a[@class='link']/@href").extract()

        urls_list = ProstituteModel.objects.filter(site_number=self.site_number).values_list('url')
        # replace used for for CYRILLIC LETTER ES
        urls_list = [item[0].replace('%D1%81', u'с').replace('%D0%A1',u'С') for item in urls_list]
        for url in urls:
            if not url in urls_list:
            # if not ProstituteModel.objects.filter(url=url).exists():
                yield scrapy.Request(url, callback=self.parse_item)
        # next_page = response.xpath("//table[3]//tr[2]/td[1]/div[4]/a[last()]/@href").extract_first() # returns once pervious page
        next_page = response.xpath(".//table[3]//tr[2]/td[1]/div[4]/div/following-sibling::*/@href").extract_first()

        if next_page is not None:
            yield scrapy.Request(next_page, callback=self.parse)

    def parse_item(self, response):
        self.logger.info('////start parsing'+response.url+'////')
        ladie = LadieItem()
        ladie['url'] = response.url
        ladie['site_number'] = self.site_number
        
        info_table = response.selector.xpath("//table[4]//tr/td[1]/table")
        ladie['name'] = info_table.xpath(".//tr[1]/td/h1/text()").extract_first()
        text = info_table.xpath(".//tr[2]/td[1]/div/text()")

        ladie['city'] = u'kyiv'
        if text[5].extract().strip().lower() == u'мужчина':
            ladie['gender'] = 2
        ladie['age'] = text[6].re(r'\d+')[0] if text[6].re(r'\d+') else ''
        ladie['weight'] = text[7].re(r'\d+')[0] if text[7].re(r'\d+') else ''
        ladie['height'] = text[8].re(r'\d+')[0] if (len(text) == 9 and text[8].re(r'\d+')) else ''

        ladie['description'] = info_table.xpath(".//tr[3]/td/div/text()").extract_first().strip()

        ladie['phone'] = info_table.xpath(".//tr[3]/td/div/div[@class='post-contacts']/span/text()").extract_first()

        image_urls = info_table.xpath(".//tr/td[2]/a/img[@class='image_galary']/@src").extract()
        image_urls = list(set(image_urls)) # remove same links
        ladie['image_urls'] = json.dumps([urlparse.urljoin(response.url, image_url) for image_url in image_urls])
        return ladie
