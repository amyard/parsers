# -*- coding: utf-8 -*-
from ..items import LadieItem
import datetime, json, re, scrapy
from scrapy.spiders import CrawlSpider, Rule, Spider, SitemapSpider
from scrapy.linkextractors import LinkExtractor
import urlparse
from scrapy import Selector
import xml.etree.ElementTree as ET
import os
from prostitute.models import ProstituteModel


class TopModalsSpider(Spider):
    name = 'top_modals_sitemap'
    allowed_domains = ['www.top-modals.com']
    site_number = 1
    start_urls = ['http://www.top-modals.com/']
    page = 1

    def parse(self, response):
        ids_list = []
        get_href = response.selector.xpath("//td[contains(@class,'fn')]")
        for nmb, id in enumerate(get_href):
            ids = id.css('a::attr(href)').extract_first()
            ids_list.append(ids.split('?id=')[1])

        urls = ['http://www.top-modals.com/?id={}'.format(item) for item in ids_list]
        urls_list = ProstituteModel.objects.filter(site_number=self.site_number).values_list('url')
        urls_list = [item[0] for item in urls_list]

        for url in urls:
            if url not in urls_list:
                yield scrapy.Request(url, callback=self.parse_item)

        new_page = response.selector.xpath("//td[contains(@class,'cPg')]/following-sibling::td[1]")
        if new_page:
            page_nmb = response.selector.xpath("//td[contains(@class,'cPg')]/following-sibling::td[1]//text()").extract()[0]
            next_page = 'http://www.top-modals.com/?p={}'.format(page_nmb)
            yield scrapy.Request(next_page, callback=self.parse)

    def parse_item(self, response):

        self.logger.info('////start parsing////')
        ladie = LadieItem()
        ladie['url'] = response.url

        print(response.url)
        ladie['site_number'] = self.site_number
        info_table = response.selector.xpath("//table[@id='blcMain']//tr/td[@class='brd']/table")[0]
        ladie['name'] = info_table.xpath(".//tr[1]/td/table//tr/td[1]/div[@class='sf1']/div[@class='sf2']/div[@class='nl2']/text()").extract_first()

        ladie['city'] = u'kyiv'
        ladie['district'] = info_table.xpath(".//tr[1]/td/table//tr/td[@class='nmfl']/a/text()").extract_first()
        ladie['phone'] = info_table.xpath("//div[contains(@class,'fl')]/text()").extract_first()

        text = info_table.xpath(".//tr/td[@class='t']")
        text1 = text.re(r"\d+")
        text2 = text.re(r"(\d+ )(\w\w\w)")
        ladie['age'] = text1[0]
        ladie['height'] = text1[1]
        ladie['weight'] = text1[2]
        ladie['bust'] = text1[3]
        data = info_table.xpath("//span[contains(@property,'price')]/text()").extract()[0]
        ladie['price_1'] = data.replace(' ', '')
        ladie['currency'] = info_table.xpath("//span[contains(@property,'price')]/following-sibling::text()").extract()[0]
        text3 = text.re(r'(?<=style="font-weight:normal">)[^<]+')
        ladie['proposes'] = json.dumps(text3[0].replace(',', '').strip().split())
        description = info_table.xpath(".//tr[3]/td[@class='d2']/text()").extract()
        ladie['description'] = '\r\n'.join(description)

        image_urls = response.xpath("//meta[@property='og:image']/@content").extract()
        ladie['image_urls'] = json.dumps(image_urls)
        return ladie


        # r_w = re.compile(r"iLST\((.+)\);")
        # self.logger.info('////start parsing////')
        # ladie = LadieItem()
        # ladie['url'] = response.url
        # ladie['site_number'] = self.site_number
        # info_table = response.selector.xpath("//td[contains(@class, 'brd')]")[1]
        # text =  info_table.extract().replace("\n", "").replace("\r", "")
        # prop = r_w.findall(text)[0].split(",")
        # exprop = re.findall(r',"(.+)"', text)
        # ladie['name'] = prop[15].replace('"', '')
        # ladie['city'] = u'kyiv'
        # ladie['district'] = prop[-2]
        # ladie['phone'] = "+380"+prop[14]
        # ladie['weight'] = prop[8]
        # ladie['height'] = prop[8]
        # ladie['bust'] = prop[7]
        # ladie['age'] = prop[8]
        # ladie['price_1'] = prop[12]
        # ladie['currency'] = ""
        # ladie['proposes'] = ""
        # ladie['description'] = "".join(prop)
        # ladie['image_urls'] = "http://www.top-modals.com/files/{}/v1.jpg?".format(prop[4])