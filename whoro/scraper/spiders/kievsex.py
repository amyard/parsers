# -*- coding: utf-8 -*-
from ..items import LadieItem
import datetime, json, re, scrapy
from scrapy.spiders import CrawlSpider, Rule, Spider, SitemapSpider
from scrapy.linkextractors import LinkExtractor
import urlparse
from scrapy import Selector
import xml.etree.ElementTree as ET
import os
from prostitute.models import ProstituteModel


class KievSexSpider(Spider):
    name = 'kievsex'
    allowed_domains = ['kievsex.biz']
    site_number = 6
    start_urls = ['http://kievsex.biz/all/',
                  # 'http://kievsex.biz/product-sitemap1.xml',
                  # 'http://kievsex.biz/product-sitemap2.xml',
                  # 'http://kievsex.biz/product-sitemap3.xml',
                  # 'http://kievsex.biz/product-sitemap4.xml'
                  ]
    url_list_mask = 'http://kievsex.biz/all/page/{}/'
    page = 1

    def parse(self, response):
        urls = response.xpath("//a[@class='viewmore']/@href").extract()
        urls_list = ProstituteModel.objects.filter(site_number=self.site_number).values_list('url')
        urls_list = [item[0] for item in urls_list]
        for url in urls:
            url = urlparse.urljoin(response.url, url)
            if url not in urls_list:
                yield scrapy.Request(url, callback=self.parse_item)
        if urls:
            self.page += 1
            # next_page = urlparse.urljoin(response.url, self.url_list_mask.format(self.page))
            next_page = self.url_list_mask.format(self.page)
            yield scrapy.Request(next_page, callback=self.parse)

        # root = ET.fromstring(response.text.encode('utf-8'))
        # urls_list = ProstituteModel.objects.filter(site_number=self.site_number).values_list('url')
        # urls_list = [item[0] for item in urls_list]
        # for href in root:
        #     url = href[0].text
        #     if url.find('girl') == -1:
        #         continue
        #     if not url in urls_list:
        #         yield scrapy.Request(url, callback=self.parse_item)

    def parse_item(self, response):
        self.logger.info('////start parsing'+response.url+'////')
        ladie = LadieItem()
        ladie['url'] = response.url
        ladie['site_number'] = self.site_number
        ladie['city'] = u'kyiv'

        info_table = response.selector.xpath("//div[@id='page-wrapper']/div[@class='page-outer']/div[@class='page-inner']/div[@id='content-wrapper']")
        ladie['name'] = info_table.xpath("./div[@id='container']/div[@id='content']/div/div[@class='summary entry-summary']/h1[@class='product_title entry-title']/text()").extract_first()

        price = info_table.xpath("./div[@id='container']/div[@id='content']/div/div[@class='summary entry-summary']/div[1]/p[@class='price']").re(r'([,\d]+)\s(\w+)')
        if price and len(price) == 2:
            ladie['price_1'] = price[0].replace(',', '')
            ladie['currency'] = price[1]
            price_2 = info_table.xpath("./div[@id='container']/div[@id='content']/div/div[@class='summary entry-summary']/p[@class='price'][1]").re(r'(\d+)')
            ladie['price_2'] = price_2[1] if price_2 and len(price_2) ==2 else ''
            price_night = info_table.xpath("./div[@id='container']/div[@id='content']/div/div[@class='summary entry-summary']/p[@class='price'][2]").re(r'(\d+)')
            ladie['price_night'] = price_night[0] if price_night else ''
        
        ladie['phone'] = info_table.xpath("./div[@id='container']/div[@id='content']/div/div[@class='summary entry-summary']/a/div[@class='tel']/text()").extract_first()

        info = info_table.xpath("./div[@id='container']/div[@id='content']/div/div[@class='summary entry-summary']/div[4]/div")
        text = re.sub(r'(\<(/?[^>]+)>)', '', info.extract_first())

        age = re.findall(ur'Возраст:\s+(\d+)', text, re.UNICODE)
        ladie['age'] = age[0] if age else ''
        height = re.findall(ur'Рост:\s+(\d+)', text, re.UNICODE)
        ladie['height'] = height[0] if height else ''
        bust = re.findall(ur'Грудь:\s+(\d+)', text, re.UNICODE)
        ladie['bust'] = bust[0] if bust else ''
        weight = re.findall(ur'Вес:\s+(\d+)', text, re.UNICODE)
        ladie['weight'] = weight[0] if weight else ''

        proposes = info_table.xpath("./div[@id='container']/div[@id='content']/div/div[@class='summary entry-summary']/div[@class='product_meta']/div[@class='tagged_as']/a/text()").extract()
        ladie['proposes'] = json.dumps(proposes)
        description = info_table.xpath("./div[@id='container']/div[@id='content']/div/div[@class='desc']/p").extract_first()
        ladie['description'] = re.sub(r'(\<(/?[^>]+)>)', '', description) if description else ''

        image_urls = info_table.xpath(".//div[@class='images']//img/@src").extract()
        image_urls = list(set(image_urls))
        ladie['image_urls'] = json.dumps(image_urls)
        return ladie
