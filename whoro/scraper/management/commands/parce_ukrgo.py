import os, scrapy
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
from django.core.management.base import BaseCommand, CommandError

from scraper.spiders.ukrgo import UkrgoSpider


class Command(BaseCommand):

    def handle(self, *args, **kwargs):
        # os.environ['SCRAPY_PROJECT'] = 'top_modails'
        process = CrawlerProcess(get_project_settings())
        process.crawl(UkrgoSpider)
        process.start()
