import os, scrapy
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
from django.core.management.base import BaseCommand, CommandError

from scraper.spiders.minuet import MinuetSpider
from scraper.spiders.ukrgo import UkrgoSpider
from scraper.spiders.top_modals import TopModalsSpider
from scraper.spiders.sexzakaz import SexzakazSpider
from scraper.spiders.relaxportal import RelaxportalSpider
from scraper.spiders.kievsex import KievSexSpider
from scraper.spiders.sexkiev import SexKievSpider
from scraper.spiders.sexua import SexUaSpider
from scraper.spiders.intimgirls import IntimGirlsSpider
from scraper.spiders.escortkiev import EscortKievSpider
from scraper.spiders.kievlady import KievLadySpider
from scraper.spiders.hotlove import HotLoveSpider
from scraper.spiders.kievgirls import KievGirlsSpider

from django.core.mail import send_mail
from datetime import datetime, date
from prostitute.models import ProstituteModel
from django.conf import settings


class Command(BaseCommand):

    def handle(self, *args, **kwargs):
        # os.environ['SCRAPY_PROJECT'] = 'top_modails'
        process = CrawlerProcess(get_project_settings())
        process.crawl(MinuetSpider)
        process.crawl(UkrgoSpider)
        process.crawl(TopModalsSpider)
        process.crawl(SexzakazSpider)
        process.crawl(RelaxportalSpider)
        process.crawl(KievSexSpider)
        process.crawl(SexKievSpider)
        process.crawl(SexUaSpider)
        process.crawl(IntimGirlsSpider)
        process.crawl(EscortKievSpider)
        process.crawl(KievLadySpider)
        process.crawl(HotLoveSpider)
        process.crawl(KievGirlsSpider)
        process.start()
        self.send_info_mail()

    def send_info_mail(self):
        count_new_objects = ProstituteModel._default_manager.filter(
                            date__startswith=date.today()
                            ).count()
        message = u'Was parcet {} new adverts in all cities'.format(count_new_objects)
        send_mail(
                'Parcers finished',
                message,
                settings.EMAIL_HOST_USER,
                settings.MANAGERS,
                fail_silently=False,
            )
