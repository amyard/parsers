import os, scrapy
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
from django.core.management.base import BaseCommand, CommandError

from scraper.spiders.relaxportal import RelaxportalSpider


class Command(BaseCommand):

    def handle(self, *args, **kwargs):
        process = CrawlerProcess(get_project_settings())
        process.crawl(RelaxportalSpider)
        process.start()
