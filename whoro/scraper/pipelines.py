# -*- coding: utf-8 -*-
from prostitute.models import ProstituteModel


class ProstitutePipeline(object):
    def process_item(self, item, spider):
        model = ProstituteModel()
        if item['name']:
            for key, value in item.items():
                if value:
                    setattr(model, key, value)
            model.save()
        return item

