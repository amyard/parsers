# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/topics/items.html

from scrapy.item import Item, Field


class LadieItem(Item):
    # price_hour = Field()
    name = Field()
    phone = Field()
    price_1 = Field()
    price_2 = Field()
    price_night = Field()
    currency = Field()
    city = Field()
    district = Field()
    proposes = Field()
    description = Field()
    weight = Field()
    height = Field()
    bust = Field()
    age = Field()
    url = Field()
    image_urls = Field()
    underground = Field()
    different = Field()
    gender = Field()
    site_number = Field()